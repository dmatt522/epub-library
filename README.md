Epub.js
================================

![FuturePress Views](http://fchasen.com/futurepress/fp.png)

Epub.js is a JavaScript library for rendering ePub documents in the browser, across many devices.

Epub.js provides an interface for common ebook functions (such as rendering, persistence and pagination) without the need to develop a dedicated application or plugin. 


Why EPUB
-------------------------

![Why EPUB](http://fchasen.com/futurepress/whyepub.png)

The [EPUB standard] is a widely used and easily convertible format.  Many books are currently in this format, and it is convertible to many other formats (such as PDF, Mobi and iBooks).

An unzipped ePUB3 is a collection of HTML5 files, CSS, images and other media – just like any other website.  However, it enforces a schema of book components, which allows us to render a book and its parts based on a controlled vocabulary.  

More specifically, the ePUB schema standardizes the table of contents, provides a manifest that enables the caching of the entire book, and separates the storage of the content from how it’s displayed.

Getting Started
-------------------------

Get the minified code from the build folder:

```html
<script src="../build/epub.min.js"></script>
```

If you plan on using compressed (zipped) epubs (any .epub file) include the minified version of [JSZip.js] which can be found in [build/libs]

```html
<!-- Zip JS -->
<script src="/build/libs/zip.min.js"></script>
```

Setup a element to render to:

```html
<div onclick="Book.prevPage();">‹</div>
<div id="area"></div>
<div onclick="Book.nextPage();">›</div>
```

Create the new ePub, and then render it to that element:

```html
<script>
	var Book = ePub("url/to/book/");
	Book.renderTo("area");
</script>
```

Internet Explorer
-------------------------

Compatibility with IE is best with wicked-good-xpath, a Google-authored pure JavaScript implementation of the DOM Level 3 XPath specification (but not required). 

```html
<script src="/examples/wgxpath.install.js"></script>
```

Then install wgxpath via a hook like the one below:

```javascript
EPUBJS.Hooks.register("beforeChapterDisplay").wgxpath = function(callback, renderer){

  wgxpath.install(renderer.render.window);

  if(callback) callback();
};

wgxpath.install(window);
```

Recent Updates
-------------------------
+ v2 splits the render method from the layout and renderer. Currently only iframe rendering is supported, but this change will allow for new render methods in the future. 

+ Work-in-progress pagination support using EPUB page-lists. ```renderer:pageChanged``` has changed to ```renderer:locationChanged``` and a ```book:pageChanged``` event was added to pass pagination events.

+ Moved [Demo Reader] to ```/reader/``` and the source to ```/reader_src/```.

+ Updated CFI handling to support text offsets. CFIs return wrapped like: ```"epubcfi(/6/12[xepigraph_001]!4/2/28/2/1:0)"```. Ranges to be added soon.

+ Added support for [EPUB properties]. This can be overridden in the settings and default to ```{spread: 'reflowable', layout: 'auto', orientation: 'auto'}```

Running Locally
-------------------------

Install [node.js](http://nodejs.org/)

Then install the project dependences with npm

```javascript
npm install
```

You can run the reader locally with the command

```javascript
node server.js
```

Builds are concatenated and minified using [gruntjs](http://gruntjs.com/getting-started)

To generate a new build run

```javascript
grunt
```

Or, to generate builds as you make changes run

```
grunt watch
```


